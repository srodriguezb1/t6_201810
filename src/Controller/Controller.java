package Controller;

import java.util.Date;

import estructuras.ListaEncadenada;
import logic.TaxisChicago;
import vo.Carrera;
import vo.Compa�ia;
import vo.Taxi;

public class Controller 
{
	/**
	 * Reference to the services manager
	 */
	private static TaxisChicago  manager = new TaxisChicago();
	
	/**
	 * M�todo que carga los servicios
	 * @param i n�mero el archivo a cargar
	 */
	public static void loadServices(int i ) 
	{		
		manager.cargar(i);
	}
	
	/**
	 * M�todo que genera una cola de servicios en un rango de fechas
	 * @param inicio fecha inicial
	 * @param fin fecha final
	 * @return cola con los servicios en orden
	 * REQEURIMIENTO 1A
	 */
	public static Carrera[] requerimiento1(String zona)
	{
		return manager.requerimiento1(zona);
	}
	
	public static ListaEncadenada<Carrera> requerimiento2(int lim)
	{
		return manager.requerimiento2(lim);
	}

	/**
	 * M�todo que retorna la informaci�n de un taxi en un rango de fechas espec�ficas
	 * @param id id del taxi
	 * @param in fecha incial
	 * @param fi fecha final
	 * @return String con la informaci�n de un taxi en un rango
	 * REQUERIMIENTO 3A
	 */
	public static String darInformacionTaxiEnRango(String id, Date in, Date fi)
	{
		return manager.darInformacionTaxiEnRango(id, in,fi);
	}

	/**
	 * M�todo que convierte una fecha ingresada por consola en un objeto de tipo Date
	 * @param fechaInicial fecha inicial
	 * @param horaInicial hora inicial
	 * @param fechaFinal fecha final
	 * @param horaFinal hora final
	 * @return arreglo de tipo date cuya primera posicion es la fecha inicial y cuya segunda posicion es la fecha final
	 */
	public static Date[] darDate(String fechaInicial, String horaInicial, String fechaFinal,String horaFinal) 
	{
		return manager.darDate( fechaInicial,  horaInicial,  fechaFinal, horaFinal);
	}

	public static Compa�ia[] requerimiento3() 
	{
		return manager.requerimiento3();
	}


}
