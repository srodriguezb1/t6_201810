package logic;

import java.util.Comparator;

import vo.Compa�ia;

public class CompararPorNombre implements Comparator<Compa�ia>
{

	/**
	 * Metodo que compara dos nombres de compa�ias
	 */
	@Override
	public int compare(Compa�ia o1, Compa�ia o2) 
	{
		int compare = o1.darNombre().compareToIgnoreCase(o2.darNombre());
		if( compare < 0)
		{
			return -1;
		}
		else if(compare> 0)
			return 1;
		else
			return 0;
	}
	
}
