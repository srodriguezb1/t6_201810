package logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;

import com.google.gson.*;

import estructuras.HashTableSeparateChaining;
import estructuras.IteradorSencillo;
import estructuras.ListaEncadenada;
import estructuras.Nodo;
import estructuras.NodoLista;
import vo.Carrera;
import vo.Compa�ia;
import vo.RangoDistancia;
import vo.Taxi;


public class TaxisChicago 
{
	/**
	 * Atributo que representa una lista de servicios
	 */
	private ListaEncadenada<Carrera> services;
	
	/**
	 * Atributo que representa una lista de taxis
	 */
 	private ListaEncadenada<Taxi> taxis;
 	
 	private ListaEncadenada<Compa�ia> compa�ias;
 	
 	private ListaEncadenada<RangoDistancia> listaRangos;
 	
 	
 	private HashTableSeparateChaining hashZona;
 	
 	private HashTableSeparateChaining hashMilla;
 	
 	//CONSTANTES
 	
 	/**
 	 * Constante que representa la ruta del archivo peque�o
 	 */
 	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
 	
 	/**
 	 * Constante que representa la ruta del archivo mediano
 	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
 	
	/**
	 * Metodo que carga el archivo
	 * @param serviceFile ruta del archivo
	 */
	public void cargarServicios (String serviceFile) 
	{
		BufferedReader reader = null;
		try
		{
			JsonParser parser = new JsonParser();
			JsonArray arr = (JsonArray) parser.parse(new FileReader(serviceFile));

			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj = (JsonObject) arr.get(i);
				

				String idCarrera = "";
				String idTaxi = "";

				double tiempoCarrera = 0;
				double distanciaTotal = 0.0;
				double areaRecogida = 0;
				int areaDejada = 0;
				double tarifaParcial = 0;
				double propinas = 0;
				double peajes = 0;
				double dineroExtra = 0;
				double tarifaTotal = 0;
				String tipoPago = "";
				String compania = "";
				double latitudRecogida = 0.0;
				double longitudRecogida = 0.0;
				String puntoRecogida = "POINT(" + longitudRecogida + "," + latitudRecogida + ")";
				double latitudDejada = 0.0;
				double longitudDejada = 0.0;
				String puntoDejada = "POINT(" + longitudDejada + "," + latitudDejada + ")";
				long pickupCensusTract = 0;
				long dropoffCensusTract = 0;
				Date fechaInicio = null;
				Date fechaFinal = null;


				if(obj.get("trip_id") != null)
				{
					idCarrera = obj.get("trip_id").getAsString();
				}
				if(obj.get("taxi_id") != null)
				{
					idTaxi = obj.get("taxi_id").getAsString();
				}
				if(obj.get("trip_start_timestamp") != null)
				{
					String fechaTemp = obj.get("trip_start_timestamp").getAsString();
					String[] temp = fechaTemp.split("-");
					String[] temp1 = temp[2].split(":");
					String [] temp2 = temp1[0].split("T");

					fechaInicio = new Date(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp2[0]), Integer.parseInt(temp2[1]), Integer.parseInt(temp1[1]));
				}
				if(obj.get("trip_end_timestamp") != null)
				{
					String fechaTemp = obj.get("trip_end_timestamp").getAsString();
					String[] temp = fechaTemp.split("-");
					String[] temp1 = temp[2].split(":");
					String [] temp2 = temp1[0].split("T");

					fechaFinal = new Date(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp2[0]), Integer.parseInt(temp2[1]), Integer.parseInt(temp1[1]));
				}
				if(obj.get("trip_seconds")!= null)
				{
					tiempoCarrera = obj.get("trip_seconds").getAsDouble();
				}
				if(obj.get("trip_miles")!= null)
				{
					distanciaTotal = obj.get("trip_miles").getAsDouble();
				}
				if(obj.get("pickup_community_area") != null)
				{
					areaRecogida = obj.get("pickup_community_area").getAsInt();
				}
				if(obj.get("dropoff_community_area") != null)
				{
					areaDejada = obj.get("dropoff_community_area").getAsInt();
				}
				if(obj.get("trip_total")!= null)
				{
					tarifaTotal = obj.get("trip_total").getAsDouble();
				}
				if(obj.get("company") != null)
				{
					compania = obj.get("company").getAsString();
				}
				if(fechaInicio == null)
				{
					fechaInicio = fechaFinal;
				}
				if(fechaFinal == null)
				{
					fechaFinal = fechaInicio;
				}

				Carrera nuevo = new Carrera(idCarrera, idTaxi, fechaInicio, fechaFinal, tiempoCarrera, distanciaTotal, areaRecogida, areaDejada, tarifaParcial, propinas, peajes, dineroExtra, tarifaTotal, tipoPago, compania, latitudRecogida, longitudRecogida, puntoRecogida, latitudDejada, longitudDejada, puntoDejada,  pickupCensusTract, dropoffCensusTract);		
				services.add(nuevo);

				hashZona.put((""+areaRecogida).trim(), nuevo);

				hashMilla.put(distanciaTotal, nuevo);

				System.out.println("1");
				
			}

			System.out.println("Size: " + services.darTamanio());
			System.out.println("SizeT : " + taxis.darTamanio());
		}

		catch (Exception e ) 
		{
			e.printStackTrace();
		}
		cargarCompanias();
	}
 	
 	/**
 	 * M�todo que se encarga de cargar el archivo large, dado que esta dividido en 7 sub archivos
 	 */
 	public void loadLarge()
 	{
 		String large[] = new String[7];
 		large[0] = "./data/taxi-trips-wrvz-psew-subset-02-02-2017.json";
 		large[1] = "./data/taxi-trips-wrvz-psew-subset-03-02-2017.json";
 		large[2] = "./data/taxi-trips-wrvz-psew-subset-04-02-2017.json";
 		large[3] = "./data/taxi-trips-wrvz-psew-subset-05-02-2017.json";
 		large[4] = "./data/taxi-trips-wrvz-psew-subset-06-02-2017.json";
 		large[5] = "./data/taxi-trips-wrvz-psew-subset-07-02-2017.json";
 		large[6] = "./data/taxi-trips-wrvz-psew-subset-08-02-2017.json";
 		
 		for (int i = 0; i < large.length; i++) 
 		{
			cargarServicios(large[i]);
		}
 	}
 	
 	/**
 	 * Metodo que envia el archivo a cargar
 	 * @param arch numero del archivo
 	 */
 	public void cargar(int arch)
 	{
 		services = new ListaEncadenada<Carrera>();
		taxis = new ListaEncadenada<Taxi>();
 		
		hashMilla = new HashTableSeparateChaining<>(10000);
		hashZona = new HashTableSeparateChaining<>(10000);
		
 		if(arch == 0)
 		{
 			cargarServicios(TaxisChicago.DIRECCION_SMALL_JSON);
 		}
 		else if(arch == 1)
 		{
 			cargarServicios(TaxisChicago.DIRECCION_MEDIUM_JSON);
 		}
 		else 
 		{
 			loadLarge();
 		}
 	}
 	
 	/**
 	 * Metodo que carga las compa�ias a partir de la lista cargada de Taxis
 	 * Ademas crea una lista de CompaniaServicios, dado que se va a usar en otros requerimentos
 	 * donde se necesita sacar los servicios de una compa�ia
 	 */
 	public void cargarCompanias()
 	{
 		compa�ias = new ListaEncadenada<>();
 	 	NodoLista<Taxi> nodoTaxi = taxis.darPrimerNodo();
 	 	CompararPorNombre compare = new CompararPorNombre();
 	 	while (nodoTaxi != null)
 		{
 	 		Taxi tax = (Taxi) nodoTaxi.darElemento();
 			if(tax != null)
 			{
 				String name = tax.darCompania();	
 				if (name == null || name.equals("") || name.length() == 0) 
 				{
 					name = "Independent Owner";
 				}
 				Compa�ia compa = new Compa�ia(name);
 				if(!compa�ias.containsC(compare,compa))
 				{
 					compa�ias.add(compa);
 				}
 				compa�ias.getCompare(compare,compa).agregarTaxi(tax);
 				compa�ias.getCompare(compare,compa).agregarCarreras(tax);
			}
			nodoTaxi = nodoTaxi.darSiguiente();
 		}
 	
 	}
 	
 	/**
 	 * R1A M�todo que retorna todos los servicios organizados
 	 */
 	public ListaEncadenada<Carrera> darServiciosEnRango(Date fechaInicial, Date fechaFinal)
 	{
 		NodoLista<Carrera> primer = services.darPrimerNodo();

 		ListaEncadenada<Carrera> paraOrdenar = new ListaEncadenada<Carrera>();

 		while(primer != null)
 		{ 
 			Carrera ca = primer.darElemento();
 			if(ca.darInicio().after(fechaInicial) && ca.darFinal().before(fechaFinal))
 			{
 				paraOrdenar.add(ca);
 			}
 			primer = primer.darSiguiente();
 		} 		
 		return paraOrdenar;
 	}
 	
 	/**
 	 * Requerimiento 1 del Taller 6, este metodo se le pasa la zona que se quiere buscar 
 	 * y va a buscar en el hash que se cargo en el metodo de cargar, de modo que lo usa para
 	 * acceder al primer nodo, que va a ser como una lista
 	 * @param zona, la zona que se quiere buscar, sobre esta al hacer get se hace el hash
 	 * @return un arreglo de Carreras con los elementos de los nodos que se encontraban en el
 	 * hash
 	 */
 	public Carrera[] requerimiento1(String zona)
 	{
 		Nodo<Integer,Carrera> carrera = hashZona.get(zona);
 		int cont = 0;
 		while(carrera != null)
 		{
 			cont ++;
 			carrera = carrera.darSiguiente();
 		}
 		Carrera[] arreglo = new Carrera[cont];
 		int i = 0;
 		carrera = hashZona.get(zona);
 		while(carrera != null)
 		{
 			arreglo[i] = (Carrera) carrera.darElemento();
 			carrera = carrera.darSiguiente();
 			i ++;
 		}
 		HeapSort comunicador = new HeapSort();
 	//	comunicador.heap(arreglo);
 		return arreglo;
 	}
 	/**
 	 * Metodo del requerimiento 2 del taller 6, que va a buscar un limite y debera devolver 
 	 * los servicios que se encontraban en ese rango de millas
 	 * @param limite
 	 * @return
 	 */
 	public ListaEncadenada<Carrera> requerimiento2(int limite)
 	{
 		cargarRangosDistancia();
 		
 		RangoDistancia rango = darRango(limite);
 		
 		Nodo nodo = hashMilla.get(rango);
 		
 		ListaEncadenada<Carrera> lista = new ListaEncadenada();
 		
 		while(nodo != null)
 		{
 			lista.add((Carrera) nodo.darElemento());
 			nodo = nodo.darSiguiente();
 		}
 		
 		return lista;
 	}
 	
 	public Compa�ia[] requerimiento3()
 	{
 		
 		return null;
 	}
 	
 	
 	/**
 	 * R2A M�todo que retorna el taxi que m�s servicios presto a una compa�ia
 	 */
 	public Taxi darTaxiConMasServiciosEnCompaniaYRango(Date fechaInicial, Date fechafinal, String compania)
 	{	
 		Compa�ia compT = new Compa�ia(compania);
 		Compa�ia comp = compa�ias.get(compT);
 		System.out.println(comp.darNombre());
 		NodoLista<Taxi> nodo = comp.darTaxis().darPrimerNodo();
 		for (int i = 0; i < comp.darTaxis().darTamanio(); i++) 
 		{
 			if(nodo != null && nodo.darSiguiente() != null && nodo.darSiguiente().darElemento().darCantidadServicios(fechaInicial, fechafinal) > nodo.darElemento().darCantidadServicios(fechaInicial, fechafinal))
 			{
 				nodo = nodo.darSiguiente();
 			}
 		}
 		return nodo.darElemento();
 	}
 	 	
 	/**
 	 *Dar suma de datos de un taxi por su compa�ia
 	 */
 	public String darInformacionTaxiEnRango(String idBuscado, Date fechaInicial, Date fechaFinal)
 	{
 		Taxi taxi = null;
 		String nombrecomp = null;
 		NodoLista<Compa�ia> iter = compa�ias.darPrimerNodo();
		while(iter!= null && taxi == null )
		{
			Compa�ia compania = iter.darElemento();
			if(compania.buscarTaxi(idBuscado)!= null)
			{
				taxi = compania.buscarTaxi(idBuscado);
				nombrecomp = compania.darNombre();
			}
			iter = iter.darSiguiente();
			
		}
		
		return "El nombre de la compa�ia es : " + nombrecomp + taxi.darSumaDeDatos(fechaInicial, fechaFinal);
 	}
 	/**
 	 * Metodo que encuentra el taxi por su id
 	 * @param idTaxi id del taxi
 	 * @return Taxi buscado
 	 */
 	public Taxi encontrarTaxiPorId(String idTaxi)
 	{
 		if (taxis == null) 
 		{
 			return null;
 		}
 		
 		NodoLista<Taxi> nodo = taxis.darPrimerNodo();
 		while(nodo != null)
 		{
 			if(nodo.darElemento().darId().equals(idTaxi))
 			{
 				return nodo.darElemento();
 			}
 			nodo = nodo.darSiguiente();
 		}
 		return null;
 	}
 	
 	/**
 	 * Metodo que convierte Strings a Date
 	 * @param fechaInicial fecha inicial
 	 * @param horaInicial hora inicial
 	 * @param fechaFinal fecha final
 	 * @param horaFinal hora final
 	 * @return arreglo con primera posicion fecha inicial y segunda posicion fecha final
 	 */
 	public Date[] darDate(String fechaInicial, String horaInicial, String fechaFinal,String horaFinal)
 	{
 		Date[] dates = new Date[2];
 		String[] fechainicio = fechaInicial.split("-");
 		String[] hInicio = horaInicial.split(":");
 		String[] hFin = horaFinal.split(":");
 		
 		Date inicialD = new Date(Integer.parseInt(fechainicio[0]), Integer.parseInt(fechainicio[1]), Integer.parseInt(fechainicio[2]),
 						Integer.parseInt(hInicio[0]), Integer.parseInt(hInicio[1]));
 		dates[0] = inicialD;
 		
 		if(fechaInicial != null)
 		{
 			
 		}
 		
 		if(fechaFinal != null)
 		{
 			String[] finalF = fechaFinal.split("-");
 			Date finalD = new Date(Integer.parseInt(finalF[0]), Integer.parseInt(finalF[1]), Integer.parseInt(finalF[2]),
 							Integer.parseInt(hFin[0]), Integer.parseInt(hFin[1]));
 			
 	 		dates[1] = finalD;
 		}
 		else
 		{
 			 Date inicialfinalD = new Date(Integer.parseInt(fechainicio[0]), Integer.parseInt(fechainicio[1]), Integer.parseInt(fechainicio[2]),
 	 								Integer.parseInt(hFin[0]), Integer.parseInt(hFin[1]));
 			 dates[1] = inicialfinalD;
 		}
 		
 		return dates;
 	}
 	
 	public Taxi[] convertirT(Comparable[] array)
 	{
 		Taxi[] tax = new Taxi[array.length];
 		
 		for (int i = 0; i < array.length; i++) 
 		{
			tax[i] = (Taxi) array[i];
		}
 		return tax;
 	}
 	
 	public Compa�ia[] convertirC(Comparable[] array)
 	{
 		Compa�ia[] compa = new Compa�ia[array.length];
 		
 		for (int i = 0; i < array.length; i++) 
 		{
			compa[i] = (Compa�ia) array[i];
		}
 		return compa;
 	}
 	
 	public void cargarRangosDistancia()
 	{
 		NodoLista<Carrera> servicios = services.darPrimerNodo();
 		listaRangos =  new ListaEncadenada<>();	
 		while(servicios != null)
 		{
 			Carrera car = servicios.darElemento();
 			
 			int mil = (int) car.darMillas();
 			RangoDistancia rango = new RangoDistancia();
 			rango.setLimineInferior(mil);
			rango.setLimiteSuperior(mil + 0.99);
 				
 			if(listaRangos.contains(rango))
 			{
 				RangoDistancia ra = listaRangos.get(rango);
 				ra.addService(car);
 				hashMilla.put(ra, car);
 			}
 			else 
 			{
 				listaRangos.add(rango);
 				rango.addService(car);
 				hashMilla.put(rango, car);
 			}
 
 			servicios = servicios.darSiguiente();
 		}

 	}
 	
 	public RangoDistancia  darRango(int limite)
 	{
 		NodoLista<RangoDistancia> actual = listaRangos.darPrimerNodo();
 		
 		while(actual != null)
 		{
 			if(actual.darElemento().getLimineInferior()== limite)
 			{
 				return actual.darElemento();
 			}
 			actual = actual.darSiguiente();
 		}
 		return null;
 	}
 	 	
}
