package logic;

import vo.Carrera;
import vo.Taxi;

public class HeapSort 
{
	static void heapify(Carrera arreglo[], int n, int i) 
	{
		int max, hijo;
		hijo = 2 * i + 1;
		max = i;
		if (hijo < n)
		{
			if (arreglo[hijo].darIdCarrera().compareTo(arreglo[hijo].darIdCarrera()) > 0)
			{
				max = hijo;
			}
		}
		if (hijo + 1 < n)
		{
			if (arreglo[hijo + 1].darIdCarrera().compareTo(arreglo[max].darIdCarrera()) > 0)
			{
				max = hijo + 1;
			}
		}
		if (max != i) 
		{
			Carrera temp = arreglo[i];
			arreglo[i] = arreglo[max];
			arreglo[max] = temp;
			heapify(arreglo, n, max);
		}
	}

	static void construirHeap(Carrera arreglo[]) 
	{
		for (int i = arreglo.length / 2 - 1; i >= 0; i--)
		{
			heapify(arreglo, arreglo.length, i);
		}
	}

	static Carrera[] heap(Carrera arreglo[]) 
	{
		construirHeap(arreglo);
		for (int i = arreglo.length - 1; i >= 1; i--) 
		{
			Carrera temp = arreglo[0];
			arreglo[0] = arreglo[i];
			arreglo[i] = temp;
			heapify(arreglo, i, 0);
		}
		return arreglo;
	}
}