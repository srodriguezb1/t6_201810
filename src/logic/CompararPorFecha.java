package logic;

import java.util.Comparator;

import vo.Carrera;

public class CompararPorFecha implements Comparator<Carrera>
{
	/**
	 * Retorna -1 si 1 antes que 2. 
	 * Retorna 1 si 1 despu�s que 2.
	 * Retorna 0 si empezaron al tiempo.
	 */
	public int compare(Carrera o1, Carrera o2) 
	{
		int rta = -2;
		if(o1.darInicio().before(o2.darInicio()))
		{
			rta = -1;
			
		}
		else if(o1.darInicio().after(o2.darInicio()))
		{
			rta = 1;
		}
		else
		{
			rta = 0;
		}
		return rta;
		
	}
}
