package view;

import java.util.Date;
import java.util.Scanner;

import Controller.Controller;
import estructuras.ListaEncadenada;
import estructuras.NodoLista;
import vo.Carrera;
import vo.Compa�ia;
import vo.Taxi;


public class taxisChicagoView 
{
	/**
	 * METODO MAIN
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		int type = 0;
		while(!fin)
		{
			printMenu();
			int option = sc.nextInt();
			Date inicio = null;
			Date last = null;
			
			switch(option)
			{
				case 1:
					printMenuCargar();
					int optionCargar = sc.nextInt();
					switch (optionCargar)
					{
						case 1:
							type = 0;
							break;
					
						case 2:
							type = 1;
							break;
					
						case 3:
							type = 2;
							break;
					}	
					
					
					//Memoria y tiempo
					long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime = System.nanoTime();

					
					//Cargar data
					Controller.loadServices(type);

					//Tiempo en cargar
					long endTime = System.nanoTime();
					long duration = (endTime - startTime)/(1000000);

					//Memoria usada
					long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

					break;
				
				case 2: // 1
					
					System.out.println("Ingrese la zona de su inter�s");
					String id= sc.next();
					
					Carrera[] carreras = Controller.requerimiento1(id.trim());
					System.out.println("Estas son las carreras en la zona buscada:");
					
					for(int i = 0; i < carreras.length; i++)
					{
						if(carreras[i] != null)
						{
							System.out.println(carreras[i].darIdCarrera());
							System.out.println(carreras[i].darTiempo() + "\n");
						}
					}
					
					break;
					
				case 3: //2
					
					System.out.println("Ingrese el rango de horas que quiere buscar, deber ser el limite inferior que esta buscando,"
							+ "como un numero entero, ejem: 3, y este dara el rango entre 3-3.99 ");
					int lim = sc.nextInt();
					ListaEncadenada<Carrera> carr = Controller.requerimiento2(lim);
					System.out.println("Estas son las carreras en el rango de tiempo que ha buscado:");
					
					NodoLista<Carrera> actual = carr.darPrimerNodo();
					
					while(actual != null)
					{
						System.out.println(actual.darElemento().darIdCarrera());
						System.out.println(actual.darElemento().darMillas());
						
						actual = actual.darSiguiente();
					}
					
					break;
					
				case 4: //3
					Compa�ia[] compa�ia = Controller.requerimiento3();
					
					for (int i = 0; i < compa�ia.length ; i++) 
					{
						if(compa�ia[i] != null)
						{
							System.out.println(compa�ia[i].darNombre() + "\n" + "El numero de taxis es"+compa�ia[i].contarTaxis() );
	//						System.out.println("El numero de carreras es  :  "+compa�ia[i].darCarreras().darTamanio());
						}
					}
				
				
			}
		}
	}	
	
	/**
	 * Metodo que imprime el men�
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data:");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2. (HashTable Separate Chaining) Obtenga una tabla o lista con todos los servicios que se inician "
				+ "en un area ordenados cronologicamente, se mostrara el id y su hora de llegada a la parada \n");
		System.out.println("3. (HashTable) Organiza servicios segun su distancia recorrida en millas");
		System.out.println("4. Hace una cola de prioridad con las compa�ias, su prioridad esta dada por el numero de servicios que tiene");
	}

	/**
	 * Metodo que imprime el men� para cargar los datos
	 */
	private static void printMenuCargar()
	{
		System.out.println("-- �Qu� fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}
}
