package vo;

import estructuras.ListaEncadenada;
import estructuras.Nodo;
import estructuras.NodoLista;


public class Compa�ia implements Comparable<Compa�ia>
{
	/**
	 * Atributo que representa una lista de taxis
	 */
	private ListaEncadenada<Taxi> listaTaxis = new ListaEncadenada<Taxi>();
	
	private ListaEncadenada<Carrera> listaCarrera = new ListaEncadenada<Carrera>();
	
	/**
	 * Atributo que representa el nombre de una compa�ia
	 */
	private String nombre;
	
	/**
	 * Metodo constructor de la clase compa�ia
	 */
	public Compa�ia(String pNombre)
	{
		nombre = pNombre;
	}
	
	/**
	 * M�todo que retorna el nombre de la compa�ia
	 */
	public String darNombre()
	{
		return nombre;
	}
	
	/**
	 * M�todo que retorna la lista de Taxis
	 */
	public ListaEncadenada<Taxi> darTaxis()
	{
		return listaTaxis;
	}
	
	public ListaEncadenada<Carrera> darCarreras()
	{
		return listaCarrera;
	}
	
	/**
	 * M�todo que agrega un taxi ingresado por par�metro
	 */
	public void agregarTaxi(Taxi paraAgregar)
	{
		listaTaxis.add(paraAgregar);
	}
	
	public void agregarCarrera(Carrera agregar)
	{
		listaCarrera.add(agregar);
	}
	
	public void agregarCarreras(Taxi taxi)
	{
		NodoLista<Carrera> actual = taxi.darCarreras().darPrimerNodo();
		while(actual != null)
		{
			agregarCarrera(actual.darElemento());
			actual = actual.darSiguiente();
		}
	}
	
	/**
	 * M�todo que retorna el n�mero de taxis que tiene una compa�ia
	 */
	public int contarTaxis()
	{
		return listaTaxis.darTamanio();
	}

	/**
	 * Metodo que compara compa�ias por cantidad de servicios
	 */
	@Override
	public int compareTo(Compa�ia o) 
	{
		if(listaCarrera.darTamanio() < o.darCarreras().darTamanio())
		{
			return 1;
		}
		else 
		{
			return -1;
		}
	}

	
	/**
	 * Metodo que busca un taxi en una compa�ia por su id
	 * @param idBuscado id buscado
	 * @return Taxi buscado
	 */
	public Taxi buscarTaxi(String idBuscado)
	{
		Taxi buscado = null;
		NodoLista<Taxi> taxis = listaTaxis.darPrimerNodo();
		while(taxis != null && buscado == null)
		{
			Taxi taxi = taxis.darElemento();
			Taxi com = new Taxi(null,idBuscado);
			if(taxi.compareTo(com)==0)
			{
				buscado = taxi;
			}
			taxis = taxis.darSiguiente();
			
		}
		return buscado;
	}

}
