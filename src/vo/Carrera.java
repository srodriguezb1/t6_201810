package vo;

import java.util.Date;

import logic.CompararPorFecha;

public class Carrera implements Comparable<Carrera>
{
	/**
	 * Atributo que representa  el Id de la carrera
	 */
	String idCarrera;
	/**
	 * Atributo que representa el Id del taxi
	 */
	String idTaxi;
	
	/**
	 * Atributo que representa la hora de inicio
	 */
	Date fechaInicio;
	
	/**
	 * Atributo que representa la hora final
	 */
	Date fechaFinal;
	
	/**
	 * Atributo que representa el tiempo empleado
	 */
	double tiempoCarrera;
	
	/**
	 * Atributo que representa las millas recorridas
	 */
	double distanciaTotal;
	
	/**
	 * Atributo que representa el �rea de recogida
	 */
	double areaRecogida;
	
	/**
	 * Atributo que representa el �rea de dejada
	 */
	Integer areaDejada;
	
	/**
	 * Atributo que representa la tarifa parcial de la carrera
	 */
	double tarifaParcial;
	
	/**
	 * Atributo que representa las propinas de la carrera
	 */
	double propinas;
	
	/**
	 * Atributo que representa los peajes de la carrera
	 */
	double peajes;
	
	/**
	 * Atributo que representa el dinero extra ganado en la carrera
	 */
	double dineroExtra;
	
	/**
	 * Atributo que representa la tarifa total de la carrera
	 */
	double tarifaTotal;
	
	/**
	 * Atributo que representa el tipo de pago del usuario
	 */
	String tipoPago;
	
	/**
	 * Atributo que representa el nombre de la compa�ia
	 */
	String compania;
	
	/**
	 * Atributo que representa la latitud de recogida
	 */
	double latitudRecogida;
	
	/**
	 * Atributo que representa la longitud de recogida
	 */
	double longitudRecogida;
	
	/**
	 * Atributo que representa el punto que une la latitud y longitud de recogida
	 */
	String puntoRecogida;
	
	/**
	 * Atributo que representa la latitud de dejada
	 */
	double latitudDejada;
	
	/**
	 * Atributo que representa la longitud de dejada
	 */
	double longitudDejada;
	
	/**
	 * Atributo que representa el punto que une la latitud y longitud de dejada
	 */
	String puntoDejada;
	
	/**
	 * Atributo que representa pickup census tract
	 */
	long pickupCensusTract;
	
	/**
	 * Atributo que representa el dropoffCensusTract
	 */
	long dropoffCensusTract;
	
	/**
	 * M�todo constructor de la clase carrera
	 */
	public Carrera(String pIdCarrera, String pIdTaxi, Date pInicio, Date pFinal, double pTiempo, double pMillas, double pAreaRecogida, Integer pAreaDejada, double pTarifaParcial, double pPropinas, double pPeajes, double pExtras, double pTarifaTotal, String pTipoPago, String pCompania, double pLatitudRecogida, double pLongitudRecogida, String pPuntoRecogida, double pLatitudDejada, double pLongitudDejada, String pPuntoDejada, long pPickupcensustract, long pDropoffcensustract)
	{
		idCarrera = pIdCarrera;
		idTaxi = pIdTaxi;
		fechaInicio = pInicio;;
		fechaFinal = pFinal;
		tiempoCarrera = pTiempo;
		distanciaTotal = pMillas;
		areaRecogida = pAreaRecogida;
		areaDejada = pAreaDejada;
		tarifaParcial = pTarifaParcial;
		propinas = pPropinas;
		peajes = pPeajes;
		dineroExtra = pExtras;
		tarifaTotal = pTarifaTotal;
		tipoPago = pTipoPago;
		compania = pCompania;
		latitudRecogida = pLatitudRecogida;
		longitudRecogida = pLongitudRecogida;
		puntoRecogida = pPuntoRecogida;
		latitudDejada = pLatitudDejada;
		longitudDejada = pLongitudDejada;
		puntoDejada = pPuntoDejada;
		pickupCensusTract = pPickupcensustract;
		dropoffCensusTract = pDropoffcensustract;
	}
	
	/**
	 * M�todo que retorna el Id de
	 * @return id
	 */
	public String darIdCarrera()
	{
		return idCarrera;
	}
	
	/**
	 *Metodo que retorna el id del taxi 
	 */
	public String darIdTaxi()
	{
		return idTaxi;
	}
	
	/**
	 * M�todo que retorna la fecha de inicio
	 */
	public Date darInicio()
	{
		return fechaInicio;
	}
	
	/**
	 * M�todo que retorna la fecha final
	 */
	public Date darFinal()
	{
		return fechaFinal;
	}
	
	/**
	 * M�todo que retorna el tiempo empleado
	 */
	public double darTiempo()
	{
		return tiempoCarrera;
	}
	
	/**
	 * M�todo que retorna la cantidad de millas
	 */
	public double darMillas()
	{
		return distanciaTotal;
	}
	
	/**
	 * M�todo que retorna el ID del area de recogida
	 */
	public double darAreaReacogida()
	{
		return areaRecogida;
	}
	
	/**
	 * M�todo que retorna el ID del area de dejada
	 */
	public Integer darAreaDejada()
	{
		return areaDejada;
	}
	
	/**
	 * M�todo que retorna el dinero recogido
	 */
	public double darTarifaTotal()
	{
		return tarifaTotal;
	}
	
	
	/**
	 * M�todo que retorna la compa�ia del taxi
	 */
	public String darCompania()
	{
		return compania;
	}

	/**
	 * Metodo que compara dos carreras por su fecha
	 */
	public int compareTo(Carrera o) 
	{
		CompararPorFecha comparador = new CompararPorFecha();
		return comparador.compare(this, o);
		
	}
}
