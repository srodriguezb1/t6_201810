package vo;

import java.util.*;

import estructuras.ListaEncadenada;
import estructuras.Nodo;
import estructuras.NodoLista;

public class Taxi implements Comparable <Taxi>
{
	/**
	 * Atributo que representa el nombre de la compa�ia
	 */
	private String compania;
	
	/**
	 * Atributo que representa el id de un taxi
	 */
	private String idTaxi;
	
	/**
	 * Atributo que representa una lista de carreras asociadas a cada Taxi
	 */
	private ListaEncadenada<Carrera> listaCarreras;
	
	/**
	 * M�todo constructor de la clase Taxi
	 */
	public Taxi(String pCompania, String pIdTaxi)
	{
		compania = pCompania;
		idTaxi = pIdTaxi;
		listaCarreras = new ListaEncadenada<Carrera>();
	}
	
	/**
	 * M�todo que retorna la compa��a de un taxi
	 */
	public String darCompania()
	{
		return compania;
	}
	
	/**
	 * M�todo que retorna el id del taxi
	 */
	public String darId()
	{
		return idTaxi;
	}
	
	/**
	 * M�todo que retorna las carreras
	 */
	public ListaEncadenada<Carrera> darCarreras()
	{
		return listaCarreras;
	}
	
	public boolean tieneCompania()
	{
		return compania.isEmpty();
	}

	/**
	 * M�todo que retorna la suma de datos de las carreras del taxi
	 * @param fechaFinal 
	 * @param fechaInicial 
	 */
	public String darSumaDeDatos(Date fechaInicial, Date fechaFinal)
	{
		double distanciaTotal = 0;
		double tiempoTotal = 0;
		double dineroTotal = 0;
		NodoLista<Carrera> iter =listaCarreras.darPrimerNodo();
		while(iter != null)
		{
			Carrera actual = iter.darElemento();
			
			distanciaTotal+= actual.darMillas();
			tiempoTotal += actual.darTiempo();
			dineroTotal += actual.darTarifaTotal();
			iter= iter.darSiguiente(); 
		}
		return "DistanciaT: " + distanciaTotal + "- TiempoT: " + tiempoTotal + "- DineroT: " + dineroTotal;		
	}
	
	/**
	 * M�todo que retorna la cantidad de servicios prestados por un taxi
	 */
	public int darCantidadServicios(Date inicio, Date fin)
	{
		int cantidadServ = 0;
		NodoLista<Carrera> iter = listaCarreras.darPrimerNodo();
		while(iter != null)
		{
			Carrera primer = iter.darElemento();
			cantidadServ++;
			iter = iter.darSiguiente();
		}
		return cantidadServ;
	}
	
	/**
	 * Metodo que compara dos taxis por su id
	 */
	public int compareTo(Taxi pTaxi) 
	{
		Integer rta = -2;
		if(idTaxi.compareTo(pTaxi.darId())==0)
		{
			rta = 0;
		}
		else if(idTaxi.compareTo(pTaxi.darId()) > 0)
		{
			rta = 1;
		}
		else if(idTaxi.compareTo(pTaxi.darId())<0)
		{
			rta = -1;
		}
		return rta;
	}
	
	/**
	 * Metodo que retorna las ganancias de un taxi en un periodo de tiempo
	 * @param inicio fecha inicial
	 * @param fin fecha final
	 * @return dinero total
	 */
	public double darGanancias(Date inicio, Date fin)
	{
		int plata = 0;
		NodoLista<Carrera> primer = listaCarreras.darPrimerNodo();
		
		while(primer != null)
		{
			Carrera e = primer.darElemento();
			if(e.darInicio().after(inicio) && e.darFinal().before(fin))
			{
				plata += primer.darElemento().darTarifaTotal();
			}
			primer = primer.darSiguiente();
		}
		return plata;
	}
	
	/**
	 * Metodo que a�ade una carrera a una lista de carreras
	 * @param elem
	 */
	public void addCarrera(Carrera elem)
	{
		listaCarreras.add(elem);
	}
	
	/**
	 * Metodo que retorna la rentabilidad de un taxis
	 * @return rentabilidad
	 */
	public double darRentabilidad()
	{
		double ganancia = 0;
		double millas = 0;
		NodoLista<Carrera>  carrera = listaCarreras.darPrimerNodo();
		while(carrera != null)
		{
			ganancia += carrera.darElemento().darTarifaTotal();
			millas += carrera.darElemento().darMillas();
			carrera = carrera.darSiguiente();
		}
		if(ganancia != 0 && millas != 0)
		{
			return ganancia/millas;
		}
		else 
			return 1;
	}
}
