package vo;

import estructuras.ListaEncadenada;

/**
 * VO utilizado en Req 5A, tiene el rango de distancia y la lista de servicios cuya distancia recorrida 
 * pertenece a dicho rango
 */
public class RangoDistancia implements Comparable<RangoDistancia>
{
	//ATRIBUTOS
	
    /**
     * Modela el valor minimo del rango
     */
	private double limiteSuperior;
	
	/**
	 * Modela el valor m�ximo del rango
	 */
	private double limiteInferior;
	
	/**
	 * Modela la lista de servicios cuya distancia recorrida esta entre el l�mite inferior y el l�mite superior
	 */
	private ListaEncadenada<Carrera> serviciosEnRango = new ListaEncadenada<>();

	//M�TODOS
	
	/**
	 * @return the limiteSuperior
	 */
	public double getLimiteSuperior()
	{
		return limiteSuperior;
	}

	/**
	 * @param limiteSuperior the limiteSuperior to set
	 */
	public void setLimiteSuperior(double limiteSuperior) 
	{
		this.limiteSuperior = limiteSuperior;
	}

	/**
	 * @return the limineInferior
	 */
	public double getLimineInferior() 
	{
		return limiteInferior;
	}

	/**
	 * @param limineInferior the limineInferior to set
	 */
	public void setLimineInferior(double limineInferior) 
	{
		this.limiteInferior = limineInferior;
	}

	/**
	 * @return the serviciosEnRango
	 */
	public ListaEncadenada<Carrera> getServiciosEnRango() 
	{
		return serviciosEnRango;
	}

	/**
	 * @param serviciosEnRango the serviciosEnRango to set
	 */
	public void setServiciosEnRango(ListaEncadenada<Carrera> serviciosEnRango)
	{
		this.serviciosEnRango = serviciosEnRango;
	}
	
	/**
	 * Metodo que a�ade una carrera
	 * @param carrera a�adir
	 */
	public void addService(Carrera carrera)
	{
		serviciosEnRango.add(carrera);
	}
	
	
	@Override
	public int hashCode()
	{
		return Integer.hashCode((int) limiteInferior);
	}

	/**
	 * Metodo que compara dos Rangos distancia
	 */
	@Override
	public int compareTo(RangoDistancia o)
	{
		
		if(limiteSuperior == o.getLimiteSuperior() && limiteInferior == o.getLimineInferior())
		{
			return 0;
		}
		else 
		{
			return -1;
		}
	}
}
