package estructuras;

import java.util.Comparator;
import java.util.Iterator;


public class ListaEncadenada < E extends Comparable<E>> implements Iterator
{
	/**
	 * Nodo que representa el primer nodo de una lista
	 */
	NodoLista<E> primerNodo;
	
	NodoLista<E> actual;
	
	/**
	 * Nodo que representa el �ltimo nodo de una lista
	 */
	NodoLista<E> ultimoNodo;
	
	/**
	 * Atributo que define el tama�o de la lista
	 */
	
	private int tamano;
	
	/**
	 * M�todo constructor de la clase ListaEncadenada
	 */
	public ListaEncadenada()
	{
		primerNodo = null;
		ultimoNodo = null;
		actual = primerNodo;
	}
	
		/**
		 * Crea el primer nodo con un elemento
		 * @param elemento
		 */
		public ListaEncadenada(E elemento)
		{
			NodoLista<E> nodo = new NodoLista<E>(elemento);
			
			primerNodo = nodo;
			
			actual = primerNodo;
			
		}
		
	/**
	 * M�todo que asigna un valor al primer nodo de la lista
	 * @param nodo para asignar
	 */
	public ListaEncadenada(NodoLista<E> nodo)
	{
		primerNodo = nodo;
		
		actual = primerNodo;
	}
	
	/**
	 * M�todo que agrega un elemento a la lista encadenada
	 * Lo agrega al final
	 */
	public void add(E paraAgregar)
	{
		NodoLista<E> nodo = new NodoLista<>(paraAgregar);
		if(primerNodo == null)
		{
			primerNodo = nodo;
			ultimoNodo = primerNodo;
			tamano = 1;
		}
		else 
		{
			ultimoNodo.cambiarSiguiente(nodo);
			ultimoNodo = nodo;
			tamano ++;
		}
	}
	
	/**
	 * Metodo que retorna el elemento de un nodo
	 * @param element buscado
	 * @return nodo que lo contiene
	 */
	public E get(E element) 
 	{
		NodoLista<E> actual = primerNodo;
 		while(actual != null)
		{
 			if(actual.darElemento().compareTo(element) == 0)
 			{
				return actual.darElemento();
 			}
			actual = actual.darSiguiente();
 		}
 		return null;
 	}
	
	public E getCompare(Comparator compare, E element)
	{
		NodoLista<E> actual = primerNodo;
 		while(actual != null)
		{
 			if(compare.compare(actual.darElemento(),element ) == 0)
 			{
				return actual.darElemento();
 			}
			actual = actual.darSiguiente();
 		}
 		return null;
	}
	
	/**
	 * M�todo que elimina un elemento ingresado por par�metro
	 */
	public void remove(E paraBorrar)
	{
		boolean borre = false;
		NodoLista<E> comunicador = primerNodo;
		while(comunicador != null && !borre)
		{
			if(comunicador.darElemento().compareTo(paraBorrar)== 0)
			{
				comunicador.cambiarElemento(null);
				borre = true;
			}
			comunicador = comunicador.darSiguiente();
		}
	}
	
	/**
	 * M�todo que retorna el tama�o de la lista
	 */
	public int darTamanio()
	{
		int rta = 0;
		NodoLista<E> comunicador = primerNodo;
		while(comunicador != null)
		{	
			comunicador= comunicador.darSiguiente(); 
			rta++;
		}
		return rta;
	}

	/**
	 * Metodo que retorna la posicion de un nodo
	 * @param position posicion
	 * @return elemento
	 */
	public E get(int position) 
 	{
 		E buscado = null;
 
 		if(primerNodo != null)
 		{
 			NodoLista<E> actual = primerNodo;
 			int pos = 0;
 			while(actual != null && pos <= position)
 			{
 				if(pos == position)
 				{
 					buscado = actual.darElemento();
 				}
 
 				pos++;
 				actual = actual.darSiguiente();
 			}
 		}
 
 		return buscado;
 
 
 	}
 
	
	/**
	 * M�todo que retorna el primer nodo de la lista
	 */
	public NodoLista<E> darPrimerNodo()
	{
		return primerNodo;
	}
	
	/**
	 * M�todo que retorna el �ltimo nodo de la lista
	 */
	public NodoLista<E> darUltimoNodo()
	{
		return ultimoNodo;
	}

	
	
	//--------------------------------
	// Metodos para la clase Cola
	//--------------------------------
	
	public void insertar(E elem) 
	{
		NodoLista<E> inser = new NodoLista<>(elem);
		if(primerNodo == null)
		{
			primerNodo = inser;
			tamano = 1;
		}
		else 
		{
			ultimoNodo.cambiarSiguiente(inser);
			ultimoNodo = inser;
			tamano ++;
		}
	}
	
	/**
	 * Metodo que inserta un nodo
	 * @param nodo para insertar
	 */
	public void insertarNodo(NodoLista<E> nodo) 
	{
		if(primerNodo == null)
		{
			primerNodo = nodo;
			tamano = 1;
		}
		else 
		{
			ultimoNodo.cambiarSiguiente(nodo);
			ultimoNodo = nodo;
			tamano ++;
		}
		
	}

	/**
	 * M�todo que retorna el primer nodo 
	 * @return primerNodo
	 */
	public E getFront()
	{
		if(primerNodo != null)
			return  primerNodo.darElemento();
		else
			return null;
	}
	
	/**
	 * M�todo que elimina 
	 * @return
	 */
	public E delete() 
	{
		E elemento = null;
		
		if(primerNodo != null)
		{
			elemento = primerNodo.darElemento();
		
			primerNodo = primerNodo.darSiguiente();
		}
		return elemento;
	}

	/**
	 * M�todo que elimina la referencia al primer y �ltimo nodo vaciando la lista
	 */
	public void makeEmpty() 
	{
		primerNodo = null;
		
		ultimoNodo = null;
		
		tamano = 0;
	}

	
	//---------------------------------
	//Metodos para la clase Pila
	//---------------------------------
	
	/**
	 * M�todo que agrega al principio de la lista
	 * @param element
	 */
	public void agregarAlPrincipio(E element) 
	{
		NodoLista<E> nodo = new NodoLista<>(element);
		if(primerNodo == null)
		{
			primerNodo = nodo;
			tamano = 1;
		}
		else 
		{
			nodo.cambiarSiguiente(primerNodo);
			primerNodo = nodo;
			tamano ++;
		}
	}
	
	/**
	 * M�todo que elimina el primer nodo de la lista
	 */
	public void eliminarPrimerNodo()
	{
		if(primerNodo != null)
		{
			primerNodo = primerNodo.darSiguiente();
			tamano --;
		}
	}
	
	/**
	 * M�todo que retorna si una lista ya contiene al objeto ingresado por parametro
	 * @param obj
	 * @return
	 */
	public boolean contains(E obj) 
	{
		boolean rta = false;
		NodoLista<E> comunicador = primerNodo;
		while(comunicador!= null && !rta)
		{	
			if(comunicador.darElemento().compareTo(obj)== 0)
			{
				rta = true;
			}
			comunicador = comunicador.darSiguiente();
		}
		return rta;
	}
	
	public boolean containsC(Comparator<E> compare, E obj)
	{
		boolean rta = false;
		NodoLista<E> comunicador = primerNodo;
		while(comunicador!= null && !rta)
		{	
			if(compare.compare(comunicador.darElemento(), obj) == 0)
			{
				rta = true;
			}
			comunicador = comunicador.darSiguiente();
		}
		return rta;
	}

	@Override
	public boolean hasNext()
	{
		
		return actual != null ;
	}

	@Override
	public Object next() 
	{
		NodoLista nodo = actual;
		actual = actual.darSiguiente();
		return nodo;
	}
	
}