package estructuras;
/**
 * Clase que maneja los nodos de una lista
 *  * @param <E> que determina la clase gen�rica E
 */
public class Nodo <K extends Comparable<K>, V extends Comparable<V>>
{
	
	private K llave;
	
	/**
	 * Atributo que representa un elemento de tipo E
	 */
	private V elemento;

	/**
	 * Atributo que representa el siguiente nodo
	 */
	private Nodo<K,V> siguiente;

	/**
	 * M�todo constructor de la clase NodoLista
	 */
	public Nodo (K pLlave, V pElemento)
	{
		llave = pLlave;
		
		elemento = pElemento;
	}

	/**
	 * M�todo que cambia el siguiente nodo
	 * @param NodoCambiar
	 * �En el modelo entra un NodoLista<E>? �Eso no ser�a un nodo para cambiar
	 * de tipo E?
	 */
	public void cambiarSiguiente(Nodo<K,V> nuevo)
	{
		siguiente = nuevo;
	}

	/**
	 * M�todo que retorna el elemento del nodo
	 */
	public V darElemento()
	{
		return elemento;
	}
	
	public K darLlave()
	{
		return llave;
	}

	/**
	 * M�todo que cambia el elemento por uno ingresado por par�metro
	 * @param NodoCambiar
	 */
	public void cambiarElemento(V cambiar)
	{
		elemento = cambiar;
	}

	/**
	 * M�todo que retorna el siguiente Nodo
	 */
	public Nodo<K,V> darSiguiente()
	{
		return  siguiente;
	}


}
