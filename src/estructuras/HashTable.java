package estructuras;

import java.util.Iterator;

public interface HashTable <K extends Comparable<K>, V extends Comparable<V>>
{
	
	public V get(K key);
	
	public V delete(K key);
	
	public Iterator<K> keys();

	void put(K key, V value);
}
