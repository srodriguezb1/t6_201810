package estructuras;

import java.util.Iterator;

public class IteradorSencillo<E extends Comparable<E>> implements Iterator<E>
{
	
	/**
	 * Atributo que hace referencia al nodo sobre el cual se va a iterar
	 */
	private NodoLista<E> actual;

	//----------------
	// Metodos 
	//----------------
	
	/**
	 * M�todo constructor de la clase Iterador Sencillo
	 */
	public IteradorSencillo(NodoLista<E> nodo)
	{
		actual = nodo;
	}
	
	
	/**
	 * Metodo que dice si hace falta elementos por recorrer en una coleccion, en este caso es una lista
	 * y va a decir si el nodo sobre el cual apunta la referencia existe
	 */
	@Override
	public boolean hasNext() 
	{

		return actual != null;
	}

	/**
	 * Una vez exista la referencia al actual, lo que se va a hacer es preguntar por su 
	 * elemento y se va a avanzar sobre la lista, al siguiente del nodo actual 
	 */
	
	@Override
	public E next() 
	{
		E elemento = null;
		if(actual != null) {
			elemento = (E)actual.darElemento();
		    actual = actual.darSiguiente();
		}
		return elemento;
	}

}
