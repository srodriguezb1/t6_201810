package estructuras;

import java.util.Iterator;

import estructuras.Nodo;

public class HashTableLinear<K extends Comparable<K>, V extends Comparable<V>>
{
	private int N;
	
	private int capacidad;
	
	private K[] keys;
	
	private V[] values;
	
	public HashTableLinear( int pCapacidad)
	{
		capacidad = pCapacidad;
		
		N = 0;
		
		keys = (K[]) new Object[capacidad];
		
		values = (V[]) new Object[capacidad];
	}
	
	public int hash(K key)
	{
		return Math.abs(key.hashCode() & 0x7fffffff% capacidad);
	}
	
	public void put(K key, V valor)
	{
		
		if(N/ capacidad > 0.5)
		{
			rehash();
		}
		int i;
		
		for (i = hash(key); keys[i] != null; i = (i + 1) % capacidad)
		{
			if(keys[i].equals(key))
			{
				values[i] = valor;
				return;
			}
		}
		keys[i] = key;
		values[i] = valor;
		N ++;
	}
	public V get(K key)
	{
		int i;
		for(i = hash(key); keys[i] != null ; i = (i + 1)%capacidad)
		{
			if(keys[i].equals(key))
			{
				return values[i];
			}
		}
		return null;
	}
	
	public V delete(K key)
	{
		V element = null;
		int i;
		for(i = hash(key); keys[i] != null ; i = (i + 1)%capacidad)
		{
			if(keys[i].equals(key))
			{
				element = values[i];
				keys[i] = null;
				values[i] = null;
			}
		}
		i = (i + 1)%capacidad;
		
		while(keys[i] != null)
		{
			K actual = keys[i];
			V vactual = values[i];
			
			keys[i] = null;
			values[i] = null;
			
			N--;
			put(actual, vactual);
			i = (i + 1)%capacidad;
		}
		N--;
		return element;
	}
	
	
	public void rehash()
	{
		HashTableLinear table = new HashTableLinear(capacidad *2);
		
		for (int i = 0; i < capacidad; i++)
		{
			if(keys[i] != null)
			{
				
				table.put(keys[i],values[i]);
			}
		}
		
		keys = (K[]) table.keys;
		
		values = (V[]) table.values;
		
		capacidad = table.capacidad;
	}
	
	public Iterator<K> keys()
	{
		ListaEncadenada<K> lista = new ListaEncadenada<>();
		
		for(int i = 0; i < keys.length; i++)
		{
			lista.add(keys[i]);
		}
		
		return lista;
	}
	
}
