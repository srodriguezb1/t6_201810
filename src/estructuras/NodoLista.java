package estructuras;
	/**
	 * Clase que maneja los nodos de una lista
	 *  * @param <E> que determina la clase gen�rica E
	 */
	public class NodoLista <E extends Comparable<E>>
	{
		/**
		 * Atributo que representa un elemento de tipo E
		 */
		private E elemento;

		/**
		 * Atributo que representa el siguiente nodo
		 */
		private NodoLista<E> siguiente;
		
		/**
		 * M�todo constructor de la clase NodoLista
		 */
		public NodoLista (E pElemento)
		{
			elemento = pElemento;
		}
		
		/**
		 * M�todo que cambia el siguiente nodo
		 * @param NodoCambiar
		 * �En el modelo entra un NodoLista<E>? �Eso no ser�a un nodo para cambiar
		 * de tipo E?
		 */
		public void cambiarSiguiente(NodoLista<E> nuevo)
		{
			siguiente = nuevo;
		}
		
		/**
		 * M�todo que retorna el elemento del nodo
		 */
		public E darElemento()
		{
			return elemento;
		}
		
		/**
		 * M�todo que cambia el elemento por uno ingresado por par�metro
		 * @param NodoCambiar
		 */
		public void cambiarElemento(E cambiar)
		{
			elemento = cambiar;
		}
		
		/**
		 * M�todo que retorna el siguiente Nodo
		 */
		public NodoLista<E> darSiguiente()
		{
			return  siguiente;
		}
		
		
	}
