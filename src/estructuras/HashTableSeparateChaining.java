package estructuras;

import java.util.Iterator;

import estructuras.Nodo;


public class HashTableSeparateChaining <K extends Comparable<K>, V extends Comparable<V>>
{
	
	/**
	 * Atributo que define la capacidad, o tama�o del arreglo
	 */
	private int capacidad;
	
	/**
	 * Atributo que define el numero de elementos que se han agregado
	 */
	private int N;
	
	
	/**
	 * Atributo que define el arreglo del hash, cada posicion del arreglo va a tener un Nodo, el 
	 * cual va a tener la referencia al siguiente y este a su siguiente y asi sucesivamente
	 */
	private Nodo<K,V>[] areaPrimaria;
	
	//
	//Costructor
	//
	
	/**
	 * Constructor, crea el objeto de la clase HashTableSeparateChaining
	 * @param M, va a ser el tama�o o capacidad del arreglo
	 */
	public HashTableSeparateChaining(int M)
	{
		capacidad = M;
		
		N = 0;
		
		areaPrimaria = new Nodo[capacidad];
	}
	
	
	/**
	 * Metodo que se encarga de hacer el hash, segun una llave
	 * va a multiplicar el hashCode de la llave por un hexadecimal, como podria ser negativo
	 * se le hace valor absoluto y se divide entre la capacidad del arreglo
	 * @param key, llave sobre la cual se va a hacer hashCode()
	 * @return un entero que va a ser la posicion del arreglo
	 */
	public int hash(K key)
	{
		return Math.abs(key.hashCode() & 0x7fffffff)% capacidad;
	}
	
	/**
	 * Metodo que se encarga de a�adir un valor al Hash
	 * Dado que este caso no voy a sobre escribir el valor de cada llave, sino que
	 * si encuentra dos llaves iguales, va a a�adir otro nodo, de primeras, y el anterior
	 * o original lo pone como siguiente, no estoy sobreescribiendo el valor de una llave
	 * @param key la llave que quiero a�adir y que esta relacionada con el valor
	 * @param valor, el objeto que voy a a�adir 
	 */
	public void put(K key, V valor)
	{
		if(N / capacidad > 0.6)
		{
			rehash();
		}
		int i = hash(key);
		Nodo<K,V> x = get(key);
		Nodo<K,V> nuevo = new Nodo<K, V>(key, valor);
		nuevo.cambiarSiguiente(x);
		areaPrimaria[i] = nuevo;
		N ++;

		x = get(key);
	//	System.out.println("El nodo INSERTADO en put es: " + x + " El hash dio: " + i + " para el key " + key);

	}
	/**
	 * Metodo que me retorna un nodo, que va a ser el que este en la posicion del arreglo
	 * y esta posicion me la va a dar el hacer hash sobre la llave
	 * @param key
	 * @return Un Nodo, puede ser null
	 */
	public Nodo<K,V> get(K key)
	{
		boolean siEsta = false;
		int i = hash(key);
		Nodo<K, V> elemento = null;		
		
	//	System.out.println("En get; " + i + " : " + areaPrimaria[i] + " con key : ***" + key + "**");
		return areaPrimaria[i];	
	}
	
	
	/**
	 * Metodo que se encarga de eliminar un nodo dentro del arreglo
	 * @param key
	 * @return el elemento,V, que contenia el nodo que fue eliminado
	 */
	public V delete(K key) 
	{
		int i = hash(key);
		boolean siEsta = false;
		
		Nodo<K,V> x = areaPrimaria[i];
		V elemento = null;
		if(key.equals(x.darLlave()))
		{
			elemento = x.darElemento();
			areaPrimaria[i] = x.darSiguiente();
			siEsta = true;
			N--;
		}
		for(x = areaPrimaria[i];x != null && !siEsta; x = x.darSiguiente())
		{
			if(x.darSiguiente() != null && key.equals(x.darSiguiente().darLlave()))
			{
				elemento = x.darSiguiente().darElemento();
				x.cambiarSiguiente(x.darSiguiente().darSiguiente());
				siEsta = true;
				N --;
			}
		}
		return elemento;
	}
	
	/**
	 * Metodo que va a duplicar el tama�o del arreglo, cuando se supere un factor de carga de 
	 *  N/M > 0.6
	 *  Este metodo se va  a llamar cada vez que se va a hacer put, sino supera no se ejecuta
	 */
	public void rehash()
	{
		
		HashTableSeparateChaining<K,V> nuevo = new HashTableSeparateChaining<>(capacidad *2);
		
	//	System.out.println("Aqui estoy");
		for(int i = 0; i < areaPrimaria.length; i++)
		{
		
			for (Nodo<K,V> x = areaPrimaria[i]; x != null ; x = x.darSiguiente()) 
			{
				K llave = x.darLlave();
				V valor = x.darElemento();
				nuevo.put(llave, valor);
			}
		}
		
		capacidad = capacidad * 2;
		
		areaPrimaria = nuevo.areaPrimaria;
	}
	
	public Iterator<K> keys()
	{
		ListaEncadenada<K> lista = new ListaEncadenada<>();
		
		for(int i = 0; i < areaPrimaria.length; i++)
		{
			lista.add(areaPrimaria[i].darLlave());
		}
		
		return lista;
	}
}
