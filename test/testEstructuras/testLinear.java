package testEstructuras;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

import estructuras.HashTableLinear;
import estructuras.HashTableSeparateChaining;

public class testLinear
{
	private HashTableLinear<Integer, String> lista1;
	
	public void setUpEscenario1()
	{
		lista1 = new HashTableLinear(7);
		
		lista1.put(1, "Primer");
		lista1.put(2, "Second");
		lista1.put(3, "Tercer");
		lista1.put(4, "Four");
		lista1.put(5, "Five");
		lista1.put(6, "Seis");
	}
	
	@Test
	public void testPut()
	{
		setUpEscenario1();
		lista1.put(5, "Cinco punto");
		assertEquals("No se agrego", "Tercer", lista1.get(3));
		assertEquals("No se agrego", "Seis", lista1.get(6));
		assertEquals("No se agrego", "Cinco punto", lista1.get(5));
		lista1.delete(4);
		assertNull("No se agrego",  lista1.get(4));
	}
	
	@Test
	public void testGet()
	{
		setUpEscenario1();
		assertEquals("No se agrego", "Tercer", lista1.get(3));
		assertEquals("No se agrego", "Seis", lista1.get(6));
		assertEquals("No se agrego", "Primer", lista1.get(1));
		assertEquals("No se agrego", "Second", lista1.get(2));
		assertEquals("No se agrego", "Four", lista1.get(4));
		assertEquals("No se agrego", "Five", lista1.get(5));
	}
	
	@Test
	public void testKeys()
	{
		setUpEscenario1();
		Iterator iter = lista1.keys();
		assertEquals("No estan las llaves", iter.next(), lista1.get(3));
	}
}
